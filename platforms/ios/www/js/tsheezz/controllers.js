angular.module('Tsheezz.controllers', [])
// Tsheezz
    .controller('TsheezzCtrl', ['$scope', 'TsheezzCamera', 'TsheezzWebService', 'TsheezzService', function($scope, TsheezzCamera, TsheezzWebService, TsheezzService) {

            $scope.LaunchQRScanner = function(){
                TsheezzCamera.scanQRCode(true);
                // TODO add scan without QR
            };

        }])
// HISTORY
    .controller('HistoryCtrl', ['$scope', 'TsheezzService', function($scope, TsheezzService) {
        $scope.FriendsList = TsheezzService.getHistory();
        $scope.unsyncUrls = TsheezzService.getUnsynchronizedUrls();

        $scope.Refresh = function(){
            console.log('refresh complete');
            TsheezzService.synchronizeUrls();
            TsheezzService.getHistory();
        };

        $scope.AddContact = function (friend) {
            TsheezzService.addContactToAddressBook(friend);
            // Refresh list
            // TODO Refresh / Callback once added to contact list
            $scope.FriendsList = TsheezzService.getHistory();

        };
    }])
// SETTINGS
    .controller('SettingsCtrl', ['$scope', '$location', 'TsheezzService', function($scope, $location, TsheezzService) {

        $scope.user = TsheezzService.getProfile();

        $scope.Disconnect = function(){

            console.log('Disconnect called');
            TsheezzService.disconnectProfile();
            $location.path( "/Login" );

        };
    }])
// LOGIN
    .controller('LoginCtrl', ['$scope', '$location', 'TsheezzService', '$timeout', 'Notification', function($scope, $location, TsheezzService, $timeout, Notification) {

        $scope.login = "";
        $scope.password = "";
        $scope.wrongInput = false;

        $scope.Connect = function () {
            if($scope.login == "" || $scope.password == "") {
                $scope.wrongInput = true;
                $timeout(function(){
                    $scope.wrongInput = false;
                },2000);
                return;
            }

            TsheezzService.getProfileByLoginPassword($scope.login, $scope.password).then(function(profile) {

                if(profile === undefined) {
                    Notification.alert('Profil non trouvé');
                    return;
                }

                console.log(profile);
                TsheezzService.setProfile(profile);
                $location.path( "/" );
            });


        };
    }]);