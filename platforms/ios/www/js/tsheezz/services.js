var TESTMODE = true;

angular.module('Tsheezz.services', [])

    .factory('Notification', function () {
        return {
            alert : function (message) {
                if (navigator && navigator.notification) {
                    navigator.notification.alert(message, null, 'Ooops...', 'Ok');
                } else {
                    window.alert(message);
                }
            }
        }
    })

    .factory('CordovaReady', function () {
        return function (fn) {
            var queue = [],
                impl = function () {
                    queue.push(Array.prototype.slice.call(arguments));
                };

            document.addEventListener('deviceready', function () {
                queue.forEach(function (args) {
                    fn.apply(this, args);
                });
                impl = fn;
            }, false);

            // Check to make sure we didn't miss the  event (just in case)
            setTimeout(function() {
                if (!resolved) {
                    if (window.cordova) d.resolve(window.cordova);
                }
            }, 3000);

            return function () {
                return impl.apply(this, arguments);
            };
        };
    })

    .factory('TsheezzService', ['CordovaReady', 'DBHelper', 'TsheezzWebService', 'Notification', function(CordovaReady, DBHelper, TsheezzWebService, Notification) {


        return {
           monitorNetworkStatus : CordovaReady(function () {

               var self = this;

               console.log('cordova ready from monitorNetworkStatus')
               var initialStatus = navigator.connection.type;
               console.log('status initial : ' + initialStatus);
               // Maybe a better way to an interval is catching an iOS event ?
               window.setInterval(function(){


                   // Status changed
                   if(initialStatus !== navigator.connection.type){
                       initialStatus = navigator.connection.type;
                       console.log('status changed : ' + initialStatus);

                       if(navigator.connection.type !== Connection.NONE){
                           self.synchronizeUrls();
                       }

                   }
               }, 5000);
           }),

           initStorage : function () {
               console.log('init storage');
               if(DBHelper.getObjectForKey('TSHEEZZ.UnsynchronizedUrls') === null){
                   DBHelper.setObjectForKey('TSHEEZZ.UnsynchronizedUrls',[]);
               }
               if(DBHelper.getObjectForKey('TSHEEZZ.History') === null){
                   DBHelper.setObjectForKey('TSHEEZZ.History',[]);
               }
           },

           // URLS
           getUnsynchronizedUrls : function() {
               return DBHelper.getObjectForKey('TSHEEZZ.UnsynchronizedUrls');
           },

           saveUnsynchronizedUrl : function(url) {
               var urls = this.getUnsynchronizedUrls();
               for(var i in urls) {
                   if(urls[i] === url) {
                       return;
                   }
               }
               urls.push(url);
               DBHelper.setObjectForKey('TSHEEZZ.UnsynchronizedUrls', urls);
           },

           synchronizeUrls : function() {
               var urls = this.getUnsynchronizedUrls();
               for(var urlIndex = 0; urlIndex < urls.length; urlIndex ++ ) {
                   TsheezzWebService.getProfile(urls[urlIndex]).then(function(profile){
                       var self = this;
                       if(profile !== undefined){
                           self.saveNewHistoryItem(profile);
                       };
                   })

               }
               // Reset sync url;
               DBHelper.setObjectForKey('TSHEEZZ.UnsynchronizedUrls', []);
                // TODO Notification system for refresh
           },

           // HISTORY
           getHistory : function() {
               return DBHelper.getObjectForKey('TSHEEZZ.History');
           },

           saveNewHistoryItem : function(newHistoryItem) {
               var tHistory = this.getHistory();
               for (var tHistoryIndex = tHistory.length - 1; tHistoryIndex >= 0; tHistoryIndex--) {
                   var profile = tHistory[tHistoryIndex];
                   if(profile.name === newHistoryItem.name){
                       tHistory.splice(tHistoryIndex,1);
                   }
               }
               tHistory.push(newHistoryItem);
               DBHelper.setObjectForKey('TSHEEZZ.History', tHistory);
           },
            updateHistoryItem : function (historyItem) {
                var tHistory = this.getHistory();
                for (var tHistoryIndex = tHistory.length - 1; tHistoryIndex >= 0; tHistoryIndex--) {
                    var profile = tHistory[tHistoryIndex];
                    if(profile.name === historyItem.name){
                        tHistory[tHistoryIndex] = historyItem;
                    }
                }
                DBHelper.setObjectForKey('TSHEEZZ.History', tHistory);
            },
            // Profile
            getProfile : function () {
                return DBHelper.getObjectForKey('TSHEEZZ.CurrentProfile');

            },

            setProfile : function(profile) {
                DBHelper.setObjectForKey('TSHEEZZ.CurrentProfile',profile);
            },

            disconnectProfile : function () {
                DBHelper.removeKey('TSHEEZZ.CurrentProfile');
            },

            isProfileSet : function () {

                var currentProfile = this.getProfile();

                if (currentProfile !== null) {
                    console.log('Logged as ' + currentProfile.name);
                    return true;
                } else {
                    console.log('Not logged');
                    return false;
                }
            },

            // Helpers
            addContactToAddressBook : function(user) {
                var dbService = this;
                console.log('adding : ' + user.name);
                    var tUser = user;
                    var email = user.email,
                        name = user.name,
                        note = 'Ajouté par Tsheezz';

                    var emailContact = new ContactField('home',email, true),
                        emails = [].push(emailContact);
                    var contact = {
                        "displayName": name,
                        "emails" : emails,
                        "note" : note
                    };
                    console.log(contact);

                    var myContact = navigator.contacts.create(contact);

                    myContact.save(function() {
                        // TODO, give discreet feedback
                        tUser.inContacts = true;
                        dbService.updateHistoryItem(tUser);
                        console.log(' ' + tUser.name + ' a été ajouté avec succès aux contacts !');
                    },function(e){
                        Notification.alert('Oups... Impossible d\'ajouter ' + tUser.name + ' à vos contacts.' + e.code);
                    });

            },

            getProfileByLoginPassword : function (login, password) {
                return TsheezzWebService.getProfileByLoginPassword(login,password);
            }

       }

    }])

    .factory('DBHelper', [function(){

        return {

            getObjectForKey : function(key){
                var json = localStorage.getItem(key);
                var objectJson = JSON.parse(json);
                return objectJson;
            },

            setObjectForKey : function(key, object) {
                var json = JSON.stringify(object);
                localStorage.setItem(key,json);
            },

            removeKey : function(key) {
                localStorage.removeItem(key);
            }
        }

    }])

    .factory('TsheezzCamera', ['CordovaReady', 'TsheezzService', 'Notification', 'TsheezzWebService', function(CordovaReady, TsheezzService, Notification, TsheezzWebService) {

       return {

           openFlash : CordovaReady(function() {

               window.plugins.flashlight.available(function(isAvailable) {

                   if (isAvailable) {

                       // switch on
                       window.plugins.flashlight.switchOn(); // success/error callbacks may be passed

                   } else {
                       alert("Flashlight not available on this device");
                   }
               });
           }),

           closeFlash : CordovaReady(function(){
               window.plugins.flashlight.available(function(isAvailable) {
                   if (isAvailable) {

                       window.plugins.flashlight.switchOff(); // success/error callbacks may be passed

                   } else {
                       Notification.alert("Le flashlight n\'est pas disponible sur votre appareil.");
                   }
               });
           }),

           scanQRCode : CordovaReady(function(useFlash) {



               console.log('opening QR scanner with flash ' + useFlash);

               var _camera = this;

               if(useFlash){
                   _camera.openFlash();
               }

               cordova.plugins.barcodeScanner.scan(

                   function (result) {
                       console.log('barcode result : ' + result);

                       if(useFlash){
                           _camera.closeFlash();
                       }

                       if(result.cancelled){
                           return;
                       }

                       if(result.format !== 'QR_CODE'){
                           Notification.alert('Ce que vous avez scanné n\'est pas un QR Code.');
                           return;
                       }
                       // TODO Check that result.text is a url format with correct Tsheezed domain
                       // No Internet Connection
                       if(navigator.connection.type === Connection.NONE) {

                           TsheezzService.saveUnsynchronizedUrl(result.text);
                           Notification.alert('Vous n\'êtes pas connecté à internet. Le tsheezzed rejoindra votre communauté dès que votre smartphone sera connecté à internet.');

                       }

                       // Internet
                       else {
                           // Save to history in LocalStorage
                           TsheezzWebService.getProfile().then(function(profile){
                               if(profile !== undefined) {

                                   TsheezzService.saveNewHistoryItem(profile);

                               } else {
                                   Notification.alert('Le QR Code n\'est pas lié à un compte Tsheezz');
                               }
                           });

                       }

                   },
                   function (error) {
                       _camera.closeFlash();
                       Notification.alert("Le scan n\' pas fonctionné. \nErreur: " + error);

                   }
               );
           })

       }

    }])

    .factory('TsheezzWebService', ['$http', function($http){

        // Changes the routes here if the web service url changes
        var rootUrl = 'http://www.tsheezz.com/API';


        var webserviceConfig  = {
            userInfoUrl : rootUrl + '/userinfo/',
            addRelationUrl : rootUrl + '/addrelation/'
        };

        var httpConfig  = {
            headers : {
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept'
            }
        };


        return {

            getProfileByLoginPassword : function(login, password){

                var promise = $http.get(webserviceConfig.userInfoUrl + login + '/' + password + '?callback=', httpConfig).
                    then(function(response) {
                        // Map the json data to a Javascript Object
                        console.log(response);
                        if(response.status === 200 && !response.data.error.state) {
                            var profile =  {
                                id :        TESTMODE ? 12     : response.data.user.id,
                                name :      TESTMODE ? 'Claude-Philippe Lievens'     : response.data.user.userName,
                                qrCode :    TESTMODE ? 'test url'     : response.data.qrcode,
                                qrImg :     TESTMODE ? 'http://www.chicagonow.com/digital-media-curator/files/2013/07/QR-code.jpg'     : response.data.user.qrcode_img,
                                // Map other properties here (status, creation date, images, etc.)
                                profileImg : 'http://api.randomuser.me/0.3/portraits/men/23.jpg'
                            };
                            return profile;
                        }
                        return undefined;
                    });
                return promise;
            },

            getProfile : function(url) {
                var wsUrl = (TESTMODE)?'http://api.randomuser.me/':'';
                //var promie =$http.get()

                if(TESTMODE) {
                    var promise = $http.get(wsUrl).
                        then(function(response) {
                            console.log(response);
                            var user = response.data.results[0].user;
                            return {
                                name : user.name.first + ' '+ user.name.last,
                                status : user.name.title,
                                email : user.email,
                                image : user.picture
                            };
                        });
                    return promise;
                }
            }
        }

    }]);