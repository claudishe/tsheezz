// TODO add ng-touch

angular.module('Tsheezz', ['ngRoute', 'ngAnimate', 'Tsheezz.services', 'Tsheezz.controllers', 'Tsheezz.filters', 'ngTouch'])
  
.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller:'TsheezzCtrl',
      templateUrl:'views/tsheezz.html'
    })
    .when('/History', {
      controller:'HistoryCtrl',
      templateUrl:'views/history.html'
    })
    .when('/Login', {
      controller:'LoginCtrl',
      templateUrl:'views/login.html'
    })
    .when('/Settings', {
      controller:'SettingsCtrl',
      templateUrl:'views/settings.html'
    })
    .when('/Help', {
      templateUrl:'views/help.html'
    })
    .otherwise({
      redirectTo:'/'
    });


  })
    .run(function (TsheezzService, $location, $rootScope, Notification, CordovaService) {
        //Bootstrap App
        CordovaService.ready.then(function(){
            console.log('Cordova ready');
            TsheezzService.initStorage();
            TsheezzService.monitorNetworkStatus();
        });


        // On route change event, redirect the user if not logged
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {

            if (! TsheezzService.isProfileSet() ) {
                if($location.path() !== '/Login') {
                    $location.path( "/Login" );
                }
            }
        });

    });

