angular.module('Tsheezz.controllers', [])
// Tsheezz
    .controller('TsheezzCtrl', ['$scope', 'TsheezzCamera', '$location',
        function($scope, TsheezzCamera, $location) {

            $scope.LaunchQRScanner = function(){

                TsheezzCamera.scanQRCode(true).then(function(profile){
                    if(profile !== undefined) {
                        console.log('switching to history ?');
                        $location.path( "/History");
                    }

                },function(error){
                    console.log('Pas de profil Tsheezz : ' + error);
                });


            };

        }])
// HISTORY
    .controller('HistoryCtrl', ['$scope', 'TsheezzService', '$rootScope', function($scope, TsheezzService) {
        $scope.FriendsList = TsheezzService.getHistory();
        $scope.unsyncUrls = TsheezzService.getUnsynchronizedUrls();

        $scope.Refresh = function(){
            TsheezzService.synchronizeUrls().then(function(){
                console.log('refresh complete');
            });
        };

        $scope.AddContact = function (friend) {
            TsheezzService.addContactToAddressBook(friend);
        };
;
        $scope.$on('HistoryUpdate', function(event, friendList) {
            console.log('history update');
            console.log(friendList);
            $scope.FriendsList = friendList;
            $scope.unsyncUrls = TsheezzService.getUnsynchronizedUrls();
            $scope.$apply();
        });
    }])
// SETTINGS
    .controller('SettingsCtrl', ['$scope', '$location', 'TsheezzService', function($scope, $location, TsheezzService) {

        $scope.user = TsheezzService.getProfile();

        $scope.Disconnect = function(){
            console.log('Disconnect called');
            TsheezzService.disconnectProfile();
            $location.path( "/Login" );

        };
    }])
// LOGIN
    .controller('LoginCtrl', ['$scope', '$location', 'TsheezzService', '$timeout', 'Notification', function($scope, $location, TsheezzService, $timeout, Notification) {

        $scope.login = "";
        $scope.password = "";
        $scope.wrongInput = false;

        $scope.createAccount = function () {
            navigator.app.loadUrl(e.target.href, { openExternal: true });
            return false;
        };

        $scope.Connect = function () {
            if($scope.login == "" || $scope.password == "") {
                $scope.wrongInput = true;
                $timeout(function(){
                    $scope.wrongInput = false;
                },2000);
                return;
            }
            // TODO set loader
            TsheezzService.getProfileByLoginPassword($scope.login, $scope.password).then(function(profile) {

                if(profile === undefined) {
                    Notification.alert('Profil non trouvé');
                    return;
                }

                console.log(profile);
                TsheezzService.setProfile(profile);
                $location.path( "/" );
            });


        };
    }]);