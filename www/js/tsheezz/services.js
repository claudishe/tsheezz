var TESTMODE = true;

angular.module('Tsheezz.services', [])

    .service('CordovaService', ['$document', '$q',
        function($document, $q) {

            var d = $q.defer(),
                resolved = false;

            var self = this;
            this.ready = d.promise;

            document.addEventListener('deviceready', function() {
                resolved = true;
                d.resolve(window.cordova);
            });

            // Check to make sure we didn't miss the
            // event (just in case)
            setTimeout(function() {
                if (!resolved) {
                    if (window.cordova) d.resolve(window.cordova);
                }
            }, 3000);
        }
    ])

    .factory('Notification', function () {
        return {
            alert : function (message) {
                if (navigator && navigator.notification) {
                    navigator.notification.alert(message, null, 'Ooops...', 'Ok');
                } else {
                    window.alert(message);
                }
            }
        }
    })

    .factory('TsheezzService', ['DBHelper', 'TsheezzWebService', 'Notification', '$rootScope', '$q', function(DBHelper, TsheezzWebService, Notification, $rootScope, $q) {


        return {
           monitorNetworkStatus : function () {

               var self = this,
                   initialStatus = navigator.connection.type;
               console.log('status initial : ' + initialStatus);
               // Maybe a better way to an interval is catching an iOS event ?
               window.setInterval(function() {
                   // Status changed
                   if(initialStatus !== navigator.connection.type){
                       initialStatus = navigator.connection.type;
                       console.log('status changed : ' + initialStatus);

                       if(navigator.connection.type !== Connection.NONE){
                           self.synchronizeUrls().then(function(){
                               console.log('sync complete !');
                           });
                       }

                   }
               }, 5000);
           },

           initStorage : function () {
               console.log('init storage');
               if(DBHelper.getObjectForKey('TSHEEZZ.UnsynchronizedUrls') === null){
                   DBHelper.setObjectForKey('TSHEEZZ.UnsynchronizedUrls',[]);
               }
               if(DBHelper.getObjectForKey('TSHEEZZ.History') === null){
                   DBHelper.setObjectForKey('TSHEEZZ.History',[]);
               }
           },

           // URLS
           getUnsynchronizedUrls : function() {
               return DBHelper.getObjectForKey('TSHEEZZ.UnsynchronizedUrls');
           },

           saveUnsynchronizedUrl : function(url) {
               var urls = this.getUnsynchronizedUrls();
               for(var i in urls) {
                   if(urls[i] === url) {
                       return;
                   }
               }
               urls.push(url);
               DBHelper.setObjectForKey('TSHEEZZ.UnsynchronizedUrls', urls);
           },

           synchronizeUrls : function() {
               var self = this,
                   urls = this.getUnsynchronizedUrls(),
                   promises = [];

               var promises = urls.map(function(url){
                   var deferred = $q.defer();

                   TsheezzWebService.getProfile(url).then(function(profile){
                       if(profile !== undefined){
                           self.saveNewHistoryItem(profile);
                       };
                       deferred.resolve(profile);
                   });
                   return deferred.promise;
               });

               return $q.all(promises).then(function(results){
                   // Reset sync url;
                   DBHelper.setObjectForKey('TSHEEZZ.UnsynchronizedUrls', []);
                   return results;
               });
           },

           // HISTORY
           getHistory : function() {
               return DBHelper.getObjectForKey('TSHEEZZ.History');
           },

           saveNewHistoryItem : function(newHistoryItem) {
               // Remove angular hashkey
               delete newHistoryItem.$$hashKey;

               var tHistory = this.getHistory();
               for (var tHistoryIndex = tHistory.length - 1; tHistoryIndex >= 0; tHistoryIndex--) {
                   var profile = tHistory[tHistoryIndex];
                   if(profile.name === newHistoryItem.name){
                       tHistory.splice(tHistoryIndex,1);
                   }
               }
               tHistory.push(newHistoryItem);
               DBHelper.setObjectForKey('TSHEEZZ.History', tHistory);
               $rootScope.$broadcast('HistoryUpdate', tHistory);
           },

           updateHistoryItem : function (historyItem) {
               // Remove angular hashkey
               delete historyItem.$$hashKey;
                var tHistory = this.getHistory();
                for (var tHistoryIndex = tHistory.length - 1; tHistoryIndex >= 0; tHistoryIndex--) {
                    var profile = tHistory[tHistoryIndex];
                    if(profile.name === historyItem.name){
                        tHistory[tHistoryIndex] = historyItem;
                    }
                }
                DBHelper.setObjectForKey('TSHEEZZ.History', tHistory);
                $rootScope.$broadcast('HistoryUpdate', tHistory);
            },
            // Profile
            getProfile : function () {
                return DBHelper.getObjectForKey('TSHEEZZ.CurrentProfile');

            },

            setProfile : function(profile) {
                DBHelper.setObjectForKey('TSHEEZZ.CurrentProfile',profile);
            },

            disconnectProfile : function () {
                DBHelper.removeKey('TSHEEZZ.CurrentProfile');
            },

            isProfileSet : function () {

                var currentProfile = this.getProfile();

                if (currentProfile !== null) {
                    console.log('Logged as ' + currentProfile.name);
                    return true;
                } else {
                    console.log('Not logged');
                    return false;
                }
            },

            // Helpers
            addContactToAddressBook : function(user) {
                var dbService = this
                    deferred = $q.defer(),
                    tUser = user,
                    email = user.email,
                    name = user.name,
                    note = 'Ajouté par Tsheezz',
                    emailContact = new ContactField('home',email, true),
                    emails = [].push(emailContact),
                    contact = {
                        "displayName": name,
                        "emails" : emails,
                        "note" : note
                    };

                console.log('adding : ' + user.name);

                var myContact = navigator.contacts.create(contact);

                myContact.save(function() {
                    tUser.inContacts = true;
                    dbService.updateHistoryItem(tUser);
                    console.log(' ' + tUser.name + ' a été ajouté avec succès aux contacts !');
                    deferred.resolve(tUser);
                },function(e){
                    Notification.alert('Oups... Impossible d\'ajouter ' + tUser.name + ' à vos contacts.' + e.code);
                    deferred.reject('Erreur lors de Contact.Save : ' + e);
                });

                return deferred.promise;

            },

            getProfileByLoginPassword : function (login, password) {
                return TsheezzWebService.getProfileByLoginPassword(login,password);
            }

       }

    }])

    .factory('DBHelper', [function(){

        return {

            getObjectForKey : function(key){
                var json = localStorage.getItem(key);
                var objectJson = JSON.parse(json);
                return objectJson;
            },

            setObjectForKey : function(key, object) {
                var json = JSON.stringify(object);
                localStorage.setItem(key,json);
            },

            removeKey : function(key) {
                localStorage.removeItem(key);
            }
        }

    }])

    .factory('TsheezzCamera', ['TsheezzService', 'Notification', 'TsheezzWebService', '$q', function(TsheezzService, Notification, TsheezzWebService, $q) {

       return {

           openFlash : function() {

               window.plugins.flashlight.available(function(isAvailable) {

                   if (isAvailable) {

                       // switch on
                       window.plugins.flashlight.switchOn(); // success/error callbacks may be passed

                   } else {
                       alert("Flashlight not available on this device");
                   }
               });
           },

           closeFlash : function(){
               window.plugins.flashlight.available(function(isAvailable) {
                   if (isAvailable) {

                       window.plugins.flashlight.switchOff(); // success/error callbacks may be passed

                   } else {
                       Notification.alert("Le flashlight n\'est pas disponible sur votre appareil.");
                   }
               });
           },

           scanQRCode : function(useFlash) {

               var deferred = $q.defer();

               console.log('opening QR scanner with flash ' + useFlash);

               var _camera = this;

               if(useFlash){
                   _camera.openFlash();
               }

               cordova.plugins.barcodeScanner.scan(

                   function (result) {
                       console.log('barcode result : ' + result);

                       if(useFlash){
                           _camera.closeFlash();
                       }

                       if(result.cancelled){
                           deferred.reject('Result cancelled');
                       }

                       if(result.format !== 'QR_CODE'){
                           Notification.alert('Ce que vous avez scanné n\'est pas un QR Code.');
                           deferred.reject('No QR code');
                       }
                       // No Internet Connection
                       if(navigator.connection.type === Connection.NONE) {

                           TsheezzService.saveUnsynchronizedUrl(result.text);
                           Notification.alert('Vous n\'êtes pas connecté à internet. Le tsheezzed rejoindra votre communauté dès que votre smartphone sera connecté à internet.');
                           deferred.reject('No internet connection');

                       }

                       // Internet
                       else {
                           // Save to history in LocalStorage
                           TsheezzWebService.getProfile().then(function(profile){
                               if(profile !== undefined) {
                                   TsheezzService.saveNewHistoryItem(profile);
                                   deferred.resolve(profile);

                               } else {
                                   deferred.reject('QR Code not linked to Tsheezz Account');
                                   Notification.alert('Le QR Code n\'est pas lié à un compte Tsheezz');
                               }
                           });

                       }

                   },
                   function (error) {
                       _camera.closeFlash();
                       Notification.alert("Le scan n\' pas fonctionné. \nErreur: " + error);
                       deferred.reject('Barcode scanner not working : ' + error);
                   }
               );

               return deferred.promise;
           }

       }

    }])

    .factory('TsheezzWebService', ['$http', function($http){

        // Changes the routes here if the web service url changes
        var rootUrl = 'http://www.tsheezz.com/API';


        var webserviceConfig  = {
            userInfoUrl : rootUrl + '/userinfo/',
            addRelationUrl : rootUrl + '/addrelation/'
        };

        var httpConfig  = {
            headers : {
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept'
            }
        };


        return {

            getProfileByLoginPassword : function(login, password){

                var promise = $http.get(webserviceConfig.userInfoUrl + login + '/' + password + '?callback=', httpConfig).
                    then(function(response) {
                        // Map the json data to a Javascript Object
                        console.log(response);
                        if(response.status === 200 && !response.data.error.state) {
                            var profile =  {
                                id :        TESTMODE ? 12     : response.data.user.id,
                                name :      TESTMODE ? 'Claude-Philippe Lievens'     : response.data.user.userName,
                                qrCode :    TESTMODE ? 'test url'     : response.data.qrcode,
                                qrImg :     TESTMODE ? 'http://www.chicagonow.com/digital-media-curator/files/2013/07/QR-code.jpg'     : response.data.user.qrcode_img,
                                // Map other properties here (status, creation date, images, etc.)
                                profileImg : 'http://api.randomuser.me/0.3/portraits/men/23.jpg'
                            };
                            return profile;
                        }
                        return undefined;
                    });
                return promise;
            },

            getProfile : function(url) {
                var wsUrl = (TESTMODE)?'http://api.randomuser.me/':'';
                //var promie =$http.get()

                if(TESTMODE) {
                    var promise = $http.get(wsUrl).
                        then(function(response) {
                            console.log(response);
                            var user = response.data.results[0].user;
                            return {
                                name : user.name.first + ' '+ user.name.last,
                                status : user.name.title,
                                email : user.email,
                                image : user.picture
                            };
                        });
                    return promise;
                }
            }
        }

    }]);